import React from 'react';
import './ModalMobile.scss';

const ModalMobile = (props) => {
  return (
    <div role="search" className="modal-mobile">
      <ul>

        <li><button>PÁGINA INICIAL</button></li>
        <li><button onClick={() => {
          props.clickCategory(1, 'Camisetas');
          props.toggleModal();
        }}>CAMISETAS</button></li>

        <li><button onClick={() => {
          props.clickCategory(2, 'Calças');
          props.toggleModal();
        }}>CALÇAS</button></li>

        <li><button onClick={() => {
          props.clickCategory(3, 'Sapatos');
          props.toggleModal();
        }}>SAPATOS</button></li>
        <li><button>CONTATO</button></li>
        
      </ul>
    </div>
  )
}

export default ModalMobile;