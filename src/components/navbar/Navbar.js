import React, { Component } from 'react';
import axios from 'axios';
import './Navbar.scss';
import ModalMobile from './modalMobile/ModalMobile';

export default class Navbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      displayModal: false
    }
  }

  clickCategory = (param, category) => {
    axios.get(`http://localhost:8888/api/V1/categories/${param}`)
      .then(res => {
        this.props.changeData(res, category)
      })
      .catch(err => { throw new Error(err)});
  }

  toggleModal = () => {
    this.setState({ displayModal: !this.state.displayModal });
  }

  render() {
    return (
      <nav className="navbar">
        
        <div role="menubar" className="navbar__top">
          <p className="navbar__top--text">
            <button className="navbar__top--btn">Acesse sua Conta</button> ou <button className="navbar__top--btn">Cadastre-se</button>
          </p>
        </div>

        <div role="banner" className="navbar__search">
        <img src="/media/hamburguer_menu.png" alt="Menu clicável." onClick={() => this.toggleModal()} className="navbar__search--menu"/>
          <img src="/media/webjump_logo.png" alt="Webjump logo." className="navbar__search--logo"/>
          <div role="search" className="navbar__search-container">
            <input type="text" className="navbar__search--input"/>
            <button className="navbar__search--btn">BUSCAR</button>
          </div>
          <img src="/media/magnifying_glass.png" alt="Lupa. Abre a aba de pesquisa ao clicar." className="navbar__search--magnifying-glass"/>
        </div>

        {!!this.state.displayModal && <ModalMobile clickCategory={this.clickCategory} toggleModal={this.toggleModal} isMobile={this.state.isMobile} />}
        <div role="search" className="navbar__buttons">
          <ul>
            <li><button>PÁGINA INICIAL</button></li>
            <li><button onClick={() => this.clickCategory(1, 'Camisetas')}>CAMISETAS</button></li>
            <li><button onClick={() => this.clickCategory(2, 'Calças')}>CALÇAS</button></li>
            <li><button onClick={() => this.clickCategory(3, 'Sapatos')}>SAPATOS</button></li>
            <li><button>CONTATO</button></li>
          </ul>
        </div>
      </nav>
    )
  }
}