import React, { Component } from 'react';
import './Filter.scss';

export default class Filter extends Component {
  // Tratando informação recebida da API
  // Obs.: Seria mais prático editar a própria API
  defineFilter = (data) => {
    let filters = '';
    filters = Object.values(data.data.filters[0]);

    if (filters[0] === 'Cor') {
      return filters = 'CORES'
    } else if (filters[0] === 'Gênero') {
      return filters = 'GÊNEROS'
    }
  }

  // Pega todas as cores dos produtos que a API enviar
  getAllColors = (data) => {
    let colors = [];
    // Retirada a última letra para suportar dados vindos da API como 
    // Preto e Preta por ex.
    let colorNames = {
      pret: '#000',
      branc: '#f6f6f6',
      laranj: '#ff7f00',
      amarel: '#f8ee00',
      ros: '#f80485',
      cinz: '#959595',
      azu: '#3d45c6',
      beg: '#ddbea7',
    }

    data.data.items.forEach(item => {
      if (Object.keys(item.filter[0])[0] === 'color') {
        // Nome da cor
        let returnedColorName = Object.values(item.filter[0])[0].toLowerCase().slice(0,-1);
        // Buscando o valor em "colorNames"
        let returnedColor = colorNames[returnedColorName];
        let needInsert = true;

        for(let i = 0; i < colors.length; i++) {
          if (Object.values(colors[i])[0] === returnedColor) {
            needInsert = false;
            break;
          }
        }

        if (needInsert) {
          colors.push({[returnedColorName]: returnedColor});
        }
      }
    })
    return colors;
  }

  // Pega todos os "tipos" dos produtos que a API enviar
  getAllTypes = (data) => {
    let types = [];
    data.data.items.forEach(item => {
      if (!types.includes(item.type)) {
        types.push(item.type);
      }
    })
    return types.sort();
  }

  // Realiza o filtro ao clicar nos retângulos de cores
  filterByColor = (data, color) => {
    let newData = [];
    data.data.items.forEach(item => {
      if (!newData.includes(Object.values(item.filter[0])[0].toLowerCase().slice(0,-1))) {
        if (Object.values(item.filter[0])[0].toLowerCase().slice(0,-1) === color) {
          newData.push(item);
        }
      }
    })
    
    this.props.changeFilteredItems(newData);
  }

  // Realiza o filtro ao clicar em Masculino / Feminino
  filterByGender = (data, gender) => {
    let newData = [];
    data.data.items.forEach(item => {
      if (item.filter[0].gender.slice(0,-1).toLowerCase() === gender.slice(0,-1)) {
        newData.push(item);
      }
    })

    this.props.changeFilteredItems(newData);
  }

  // Realiza o filtro ao escolher um "Tipo"
  filterByType = (data, type) => {
    let newData = [];

    data.data.items.forEach(item => {
      if (item.type === type) {
        newData.push(item);
      }
    })

    this.props.changeFilteredItems(newData);
  }

  // Retorna os retângulos de cores
  displayColors = (data, colors) => {
    return (
      colors.map((item, idx) => {
        let colorValue = Object.values(item)[0];
        let colorName = Object.keys(item)[0];
        return <li
            title={colorName}
            className="filter__colors--display" 
            style={{backgroundColor: colorValue}} 
            key={idx}
            onClick={() => this.filterByColor(data, colorName)}>
          </li>
      })
    )
  }

  // 2 Opções: Ou retorna o filtro de cores, ou retorna o filtro de gênero
  displayFilters = (data, colors, filters) => {
    if (filters === 'CORES') {
      return (
        <ul className="filter__colors">
          { this.displayColors(data, colors)}
        </ul>
      )
    } else if (filters === 'GÊNEROS') {
      return (
        <ul className="filter__list">
          <li onClick={() => this.filterByGender(data, 'masculino')}>Masculino</li>
          <li onClick={() => this.filterByGender(data, 'feminino')}>Feminino</li>
        </ul>
      )
    }
  }

  // Exibe o filtro por Tipos
  displayTypes = (data, types) => {
    if (!types) {
      return null;
    } else {
      return (
        types.map((item, idx) => {
          return <li key={idx} onClick={() => {
            this.filterByType(data, item);
            window.scrollTo({
              top: 0,
              left: 0,
              behavior: 'smooth'
            });
          }}>{item}</li>
        })
      )
    }
  }

  render() {
    let filters, colors, types;
    let { data } = this.props.data;
    
    if (this.props.canRender) {
      filters = this.defineFilter(data);
      colors = this.getAllColors(data);
      types = this.getAllTypes(data);
    }

    return (
      <section aria-labelledby="filtro" className="filter">
        <h3 id="filtro" className="filter__title">FILTRE POR</h3>
        <h4 className="filter__subtitle">CATEGORIAS</h4>
        <ul className="filter__list">
          <li>Roupas</li>
          <li>Sapatos</li>
          <li>Acessórios</li>
        </ul>

        {!!this.props.canRender && <h4 className="filter__subtitle">{filters}</h4>}
        {this.displayFilters(data, colors, filters)}

        <h4 className="filter__subtitle">TIPO</h4>
        <ul className="filter__list">
          { this.displayTypes(data, types) }
        </ul>        
      </section>
    )
  }
}