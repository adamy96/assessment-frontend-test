import React, { Component } from 'react';
import './Shop.scss';

export default class Shop extends Component {
  // Mostra os items que estão sendo vendidos
  displayItems = (myData, itemsPerPage) => {
    if (this.props.canRender) {
      const { page } = this.props.data;
      let start = itemsPerPage * (page - 1);
      let end = itemsPerPage * page;
      myData = myData.slice(start, end);

      return (
        myData.map((item, idx) => {
          return (
            <div role="cell" className="shop__item" key={idx}>
              <img src={item.image} alt="Item a venda." className="shop__item--img" />
              <h4 className="shop__item--name">{item.name}</h4>

              {item.specialPrice ?
                <p className="shop__item--price">
                  <span className="shop__item--price-promo">R${item.price.toFixed(2)}</span>
                  R${item.specialPrice.toFixed(2)}
                </p> :
                <p className="shop__item--price">R${item.price.toFixed(2)}</p>
              }

              <button className="shop__item--btn">COMPRAR</button>
            </div>
          )
        })
      )
    }
  }

  // Determina se deve utilizar os dados "inteiros" ou os dados filtrados
  selectData = (data, filteredItems) => {
    if (filteredItems.length === 0) {
      return data.data.items;
    } else {
      return filteredItems;
    }
  }

  getItemsPerPage = () => {
    if (window.innerWidth <= 768 && window.innerWidth > 430) {
      return 9; // IPAD
    } else if (window.innerWidth <= 430) {
      return 8; // MOBILE
    } else {
      return 12 // DESKTOP, IPAD PRO
    }
  }
  
  calcNumberOfPages = (myData, itemsPerPage) => {
    return Math.ceil(myData.length / itemsPerPage);
  }

  // Paginação
  displayPages = (numberOfPages) => {
    if (numberOfPages) {
      let arrayOfPages = [...Array(numberOfPages).keys()].map((x, idx) => idx + 1);

      return (
        arrayOfPages.map((page, idx) => {
          return (
            page === this.props.data.page ?
              <li
                aria-label='Current Page.'
                role="link"
                className="shop__pagination--page"
                key={idx}
                style={{ color: '#CC0D1F', fontWeight: 800 }}
              >
                {page}
              </li> :
              <li
                role="link"
                className="shop__pagination--page"
                key={idx}
                onClick={() => {
                  this.props.changePage(page);
                  window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                  });
                }}
              >
                {page}
              </li>
          )
        })
      )
    }
  }

  // Irá atualizar o State.page do App.js
  navigationArrow = (direction, numberOfPages) => {
    if (direction === 'left') {
      if (this.props.data.page > 1) {
        this.props.changePage(this.props.data.page - 1);
        window.scrollTo({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
      }
    } else {
      if (this.props.data.page < numberOfPages) {
        this.props.changePage(this.props.data.page + 1);
        window.scrollTo({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
      }
    }
  }

  render() {
    let { data, filteredItems, category } = this.props.data;
    let itemsPerPage = this.getItemsPerPage();
    let myData, numberOfPages;

    if (this.props.canRender) {
      myData = this.selectData(data, filteredItems);
      numberOfPages = this.calcNumberOfPages(myData, itemsPerPage);
    }

    return (
      <section role="main" className="shop">
        <h3 className="shop-title">{category}</h3>

        <div aria-label="Ordenar" className="shop__sort">
          <div aria-label="Layout" className="shop__sort--icons">
            <img src="/media/grid_1.png" alt="Grid que ordena os items que estão sendo vendidos na horizontal ao clicar." />
            <img src="/media/grid_2.png" alt="Grid que ordena os items que estão sendo vendidos na vertical ao clicar." />
          </div>
          <div aria-label="Critério" className="shop__sort--selector">
            <label htmlFor="sortby">ORDERNAR POR</label>
            <select id="sortby" name="order">
              <option value="preço">Preço</option>
              <option value="ordem alfabética">Ordem Alfabética</option>
            </select>
          </div>
        </div>

        <div role="table" className="shop__display">
          {this.displayItems(myData, itemsPerPage)}
        </div>

        <nav className="shop__pagination">
          <ul>
            <img src="/media/arrow_left.png" alt="Flecha para a esqueda. Volta uma página ao clicar." className="shop__pagination--arrow" onClick={() => this.navigationArrow('left', numberOfPages)} />
            {this.displayPages(numberOfPages)}
            <img src="/media/arrow_right.png" alt="Flecha para a direita. Avança uma página ao clicar." className="shop__pagination--arrow" onClick={() => this.navigationArrow('right', numberOfPages)} />
          </ul>
        </nav>
      </section>
    )
  }
}