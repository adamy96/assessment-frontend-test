import React, { Component, Fragment } from 'react';
import axios from 'axios';
import './App.scss';
import './sass/base.scss';
import './sass/mixins.scss';
import './sass/utilities.scss';
import './sass/variables.scss';
import Navbar from './components/navbar/Navbar';
import Filter from './components/filter/Filter';
import Shop from './components/shop/Shop';
import Footer from './components/footer/Footer';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: '',
      page: 1
    }
  }

  // Setando para mostrar as camisetas caso o usuário não clique em nenhuma categoria
  componentDidMount() {
    axios.get('http://localhost:8888/api/V1/categories/1')
      .then(res => {
        this.setState({
          data: res,
          category: 'Camisetas',
          filteredItems: [],
          currentPage: 1,
        })
      })
      .catch(err => { throw new Error(err)});
  }

  changePage = (page) => {
    this.setState({
      page: page
    });
  }

  changeData = (data, category) => {
    this.setState({
      data: data,
      category: category,
      filteredItems: [],
      page: 1
    });
  }

  changeFilteredItems = (items) => {
    this.setState({
      filteredItems: items,
      page: 1
    });
  }

  render() {
    let canRender = false;
    if (this.state.data !== '') {
      canRender = true;
    }

    return (
      <Fragment>
        <Navbar changeData={this.changeData} />
        <p className="category-selected">
          Página inicial
          &nbsp;
          <span className="u-color-light-grey-1">></span>
          &nbsp;
          <span className="u-color-red">{this.state.category}</span>
        </p>
        <div aria-label="Loja" className="shop-container">
          <Filter data={this.state} canRender={canRender} changeFilteredItems={this.changeFilteredItems} />
          <Shop data={this.state} canRender={canRender} changePage={this.changePage}/>
        </div>
        <Footer />
      </Fragment>
    )
  }
}

export default App;